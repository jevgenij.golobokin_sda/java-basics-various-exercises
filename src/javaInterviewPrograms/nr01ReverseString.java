package javaInterviewPrograms;

public class nr01ReverseString {
    public static void main(String[] args) {

        // solution 1 using StringBuilder
        String inputString = "Please reverse me";
        StringBuilder stringBuilder = new StringBuilder(inputString);

        StringBuilder outputString = stringBuilder.reverse();

        System.out.println(outputString);


        // solution 2 using iteration and char array
        String inputString2 = "Reverse me again";
        char[] charArray = inputString2.toCharArray();
        for (int i = inputString2.length() - 1; i >= 0; i--) {
            System.out.print(charArray[i]);
        }

    }
}
