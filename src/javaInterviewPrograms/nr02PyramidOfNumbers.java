package javaInterviewPrograms;

public class nr02PyramidOfNumbers {
    public static void main(String[] args) {

        int numberOfRows = 5;
        int rowCounter = 1;
        for (int i = numberOfRows; i > 0; i--) {
            for (int j = 1; j <= i; j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= rowCounter; k++) {
                System.out.print(rowCounter + " ");
            }
            System.out.println();
            rowCounter++;
        }
    }
}
