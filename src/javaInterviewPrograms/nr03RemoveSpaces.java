package javaInterviewPrograms;

public class nr03RemoveSpaces {
    public static void main(String[] args) {

        //using regex and replaceAll String method
        String inputString = "R e m o v e s p a c e s";

        String stringWithNoSpaces = inputString.replaceAll("\\s+", "");

        System.out.println(stringWithNoSpaces);

        //iterating through charArray
        String input = "r e  m ooo ve uss!";
        char[] stringArray = input.toCharArray();
        String outputStringWithNoSpaces = "";
        for (int i = 0; i < input.length(); i++) {
            if (stringArray[i] != ' ') {
                outputStringWithNoSpaces = outputStringWithNoSpaces + stringArray[i];
            }
        }
        System.out.println(outputStringWithNoSpaces);
    }
}
