package javaInterviewPrograms;

import java.util.Arrays;

public class nr05CheckEqualityOfArrays {
    public static void main(String[] args) {
        int[] intArray1 = {1, 56, 88, 64};
        int[] intArray2 = {1, 56, 88, 64};
        int[] intArray3 = {1, 56, 99, 64};

        checkEqualityOfIntArrays(intArray1, intArray3);
        checkEqualityOfIntArrays(intArray1, intArray2);

        String[] stringArray1 = {"Hi", "Boom", "no"};
        String[] stringArray2 = {"Hi", "Boom", "yes"};
        String[] stringArray3 = {"Hi", "Boom", "no"};

        checkEqualityOfStringArrays(stringArray1, stringArray2);
        checkEqualityOfStringArrays(stringArray1, stringArray3);
    }

    public static void checkEqualityOfIntArrays(int[] array1, int[] array2) {
        boolean arraysAreEqual = true;
        if (array1.length == array2.length) {

            for (int i = 0; i < array1.length; i++) {
                if (array1[i] != array2[i]) {
                    arraysAreEqual = false;
                }
            }

        } else {
            arraysAreEqual = false;
        }

        if (arraysAreEqual) {
            System.out.println("Arrays are equal");
        } else {
            System.out.println("Arrays are not equal");
        }
    }

    public static void checkEqualityOfStringArrays(String[] array1, String[] array2) {
        boolean arraysAreEqual = Arrays.equals(array1, array2);

        // if to check with elements in different positions can use Arrays.sort(array) method
        //   and then check

        if (arraysAreEqual) {
            System.out.println("Arrays are equal");
        } else {
            System.out.println("Arrays are not equal");
        }
    }
}
