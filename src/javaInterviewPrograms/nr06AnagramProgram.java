package javaInterviewPrograms;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class nr06AnagramProgram {
    public static void main(String[] args) {
        checkIfAnagramWithArrays("ASTRONOMERS ", "NO MORE STARS");
        checkIfAnagramWithStringIteration("Debit Card", "Bad Credit");
        checkIfAnagramUsingHashMap("SiLeNt CAT", "LisTen AcT");

        checkIfAnagramUsingHashMap("Toss", "Shot");
    }

    public static void checkIfAnagramWithArrays(String mainString, String stringToCheck) {

        //removing spaces and going to lowercase to avoid checking spaces and case sensitive chars
        String mainStringCopy = mainString.replaceAll("\\s+", "").toLowerCase();
        String stringToCheckCopy = stringToCheck.replaceAll("\\s+", "").toLowerCase();

        boolean isAnagram = true;

        if (stringToCheckCopy.length() != mainStringCopy.length()) {
            isAnagram = false;
        } else {
            char[] mainStringArray = mainStringCopy.toCharArray();
            Arrays.sort(mainStringArray);
            char[] stringToCheckArray = stringToCheckCopy.toCharArray();
            Arrays.sort(stringToCheckArray);

            isAnagram = Arrays.equals(mainStringArray, stringToCheckArray);
        }

        if (isAnagram) {
            System.out.println("Strings are anagrams");
        } else {
            System.out.println("Strings are not anagrams");
        }

    }


    public static void checkIfAnagramWithStringIteration(String mainString, String stringToCheck) {
        String mainStringCopy = mainString.replaceAll("\\s+", "").toLowerCase();
        String stringToCheckCopy = stringToCheck.replaceAll("\\s+", "").toLowerCase();

        boolean isAnagram = true;

        if (stringToCheckCopy.length() != mainStringCopy.length()) {
            isAnagram = false;
        } else {
            char[] mainStringArray = mainStringCopy.toCharArray();
            for (char c : mainStringArray) {
                int charIndexInStringToCheck = stringToCheckCopy.indexOf(c);
                if (charIndexInStringToCheck != -1) {
                    stringToCheckCopy = stringToCheckCopy.substring(0, charIndexInStringToCheck) +
                            stringToCheckCopy.substring(charIndexInStringToCheck + 1, stringToCheckCopy.length());
                } else {
                    isAnagram = false;
                    break;
                }
            }
        }

        if (isAnagram) {
            System.out.println("Strings are anagrams");
        } else {
            System.out.println("Strings are not anagrams");
        }
    }

    private static void checkIfAnagramUsingHashMap(String mainString, String stringToCheck) {
        String mainStringCopy = mainString.replaceAll("\\s+", "").toLowerCase();
        String stringToCheckCopy = stringToCheck.replaceAll("\\s+", "").toLowerCase();

        boolean isAnagram = true;

        if (mainStringCopy.length() == stringToCheckCopy.length()) {

            Map<Character, Integer> mainStringMap = new HashMap<>();

            char[] mainStringArray = mainStringCopy.toCharArray();
            char[] stringToCheckArray = stringToCheckCopy.toCharArray();

            for (char c : mainStringArray) {
                if (mainStringMap.containsKey(c)) {
                    mainStringMap.replace(c, mainStringMap.get(c) + 1);
                } else {
                    mainStringMap.put(c, 1);
                }
            }

            for (char c : stringToCheckArray) {
                if (mainStringMap.containsKey(c)) {
                    mainStringMap.replace(c, mainStringMap.get(c) - 1);
                } else {
                    mainStringMap.put(c, 1);
                }
            }

            for (Character c : mainStringMap.keySet()) {
                if (mainStringMap.get(c) != 0) {
                    isAnagram = false;
                }
            }

            if (isAnagram) {
                System.out.println("Strings are anagrams");
            } else {
                System.out.println("Strings are not anagrams");
            }

        }
    }
}
