package javaInterviewPrograms;

public class nr07ArmstrongNumberChecker {

    public static void main(String[] args) {
        checkIfNumberIsArmstrong(153);
        checkIfNumberIsArmstrong(371);
        checkIfNumberIsArmstrong(54748);
        checkIfNumberIsArmstrong(226);
    }


    private static void checkIfNumberIsArmstrong(int number) {
        int originalNumber = number;

        int numberLength = String.valueOf(number).length();
        int sum = 0;

        while (number != 0) {
            int lastDigit = number % 10;
            sum += Math.pow(lastDigit, numberLength);
            number /= 10;
        }

        if (sum == originalNumber) {
            System.out.println("This number is Armstrong number");
        } else {
            System.out.println("Number is not Armstrong number");
        }
    }
}
