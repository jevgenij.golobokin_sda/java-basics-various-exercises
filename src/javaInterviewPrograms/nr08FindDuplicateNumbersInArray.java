package javaInterviewPrograms;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class nr08FindDuplicateNumbersInArray {

    public static void main(String[] args) {

        int[] inputArray = new int[]{111, 333, 555, 777, 333, 444, 555};
        System.out.println("==== hashset check");
        findDuplicatesWithHashSet(inputArray);
        System.out.println("==== iteration check");
        findDuplicatesWithIteration(inputArray);
        System.out.println("==== streams check");
        findDuplicatesWithStreams(inputArray);

    }

    public static void findDuplicatesWithIteration(int[] inputArray) {

        for (int i = 0; i < inputArray.length; i++) {
            for (int j = i + 1; j < inputArray.length; j++) {

                if (inputArray[i] == inputArray[j] && i != j) {
                    System.out.println("Duplicate element: " + inputArray[i]);
                }
            }
        }
    }

    public static void findDuplicatesWithHashSet(int[] inputArray) {
        HashSet<Integer> set = new HashSet<>();

        for (int element : inputArray) {
            if (!set.add(element)) {
                System.out.println("Duplicate element: " + element);
            }
        }
    }


    public static void findDuplicatesWithStreams(int[] inputArray) {
        Set<Integer> uniqueElements = new HashSet<>();
        Set<Integer> duplicateElements = Arrays.stream(inputArray)
                .filter(i -> !uniqueElements.add(i))
                .boxed()
                .collect(Collectors.toSet());
        System.out.println("Duplicate elements: " + duplicateElements);
    }


}
