package javaInterviewPrograms;

public class nr09sumOfAllDigitsOfNumber {

    public static void main(String[] args) {
        System.out.println(sumOfDigits(6455));
    }

    private static int sumOfDigits(int number) {
        int sum = 0;
        while (number != 0) {
            sum += number % 10;
            number /=  10;
        }
        return sum;
    }

}
