package javaInterviewPrograms;

import java.util.Arrays;

public class nr10secondLargestNumberOfArray {
    public static void main(String[] args) {

        int[] numbers = {3, 5, 15, 14, 15, 9, 3};

        System.out.println(findSecondLargestNumberInArray(numbers));

    }

    private static int findSecondLargestNumberInArray(int[] numbers) {
        Arrays.sort(numbers);
        int secondLargestNumber = 0;
        for (int i = numbers.length - 1; i >= 0; i--) {
            if (numbers[i - 1] < numbers[i]) {
                secondLargestNumber = numbers[i - 1];
                break;
            }
        }
        return secondLargestNumber;
    }

//    private static int findSecondLargestNumberBasic(int[] numbers) {
//        int firstLargestNumber;
//        int secondLargestNumber;
//
//        if (numbers.length < 2) {
//            System.out.println("not able to search for second largest number!");
//        }
//
//        if (numbers[0] > numbers[1]) {
//            firstLargestNumber = numbers[0];
//            secondLargestNumber = numbers[1];
//        } else if (numbers[0] < numbers[1]) {
//            firstLargestNumber = numbers[1];
//            secondLargestNumber = numbers[0];
//        } else if (numbers[0] == numbers[1] && numbers[1] == numbers[numbers.length]) {
//            System.out.println("all numbers equal!");
//        }
//
//
//        for (int i = 2; i < numbers.length; i++) {
//            if (numbers[i] > firstLargestNumber) {
//
//            }
//        }
//    }

}

