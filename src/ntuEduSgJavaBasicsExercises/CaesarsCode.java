package ntuEduSgJavaBasicsExercises;

import java.util.Scanner;


public class CaesarsCode {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        boolean inputSelectionIsValid = false;

        System.out.println("Hello! This is a Caesar's Code Machine. Would you like to (enter number):");

        do {
            System.out.println("1. Cipher your message");
            System.out.println("2. Decipher message");

            String inputSelection = scanner.next();
            scanner.nextLine(); //consumes unseen /n character

            if (inputSelection.equals("1") || inputSelection.equals("1.")) {
                cipherCaesarsCode();
                inputSelectionIsValid = true;

            } else if (inputSelection.equals("2") || inputSelection.equals("2.")) {
                decipherCaesarsCode();
                inputSelectionIsValid = true;

            } else {
                System.out.println("Invalid selection. Please enter 1 or 2.");
            }
        } while (!inputSelectionIsValid);
    }

    public static void cipherCaesarsCode() {
        System.out.println("Enter the message you want to cipher:");
        String enteredMessage = scanner.nextLine();
        StringBuilder cipheredMessage = new StringBuilder();

        for (int i = 0; i < enteredMessage.length(); i++) {
            String oldChar = enteredMessage.substring(i, i + 1);
            boolean charIsAtoW = oldChar.matches("[a-wA-W]");
            boolean charIsXYZ = oldChar.matches("[x-zX-Z]");
            boolean charIsOtherSymbols = !charIsAtoW && !charIsXYZ;

            char newChar = 0;

            if (charIsAtoW) {
                newChar = (char) (enteredMessage.charAt(i) + 3);
            }
            if (charIsXYZ) {
                newChar = (char) (enteredMessage.charAt(i) - 23);
            }
            if (charIsOtherSymbols) {
                newChar = enteredMessage.charAt(i);
            }

            cipheredMessage.append(newChar);

        }

        System.out.print(cipheredMessage);
    }

    public static void decipherCaesarsCode() {
        System.out.println("Enter the message you want to decipher:");
        String enteredMessage = scanner.nextLine();
        StringBuilder decipheredMessage = new StringBuilder();

        for (int i = 0; i < enteredMessage.length(); i++) {
            String oldChar = enteredMessage.substring(i, i + 1);
            boolean charIsABC = oldChar.matches("[a-cA-C]");
            boolean charIsDtoZ = oldChar.matches("[a-zD-Z]");
            boolean charIsOtherSymbols = !charIsABC && !charIsDtoZ;

            char newChar = 0;

            if (charIsABC) {
                newChar = (char) (enteredMessage.charAt(i) + 23);
            }
            if (charIsDtoZ) {
                newChar = (char) (enteredMessage.charAt(i) - 3);
            }
            if (charIsOtherSymbols) {
                newChar = enteredMessage.charAt(i);
            }

            decipheredMessage.append(newChar);

        }

        System.out.print(decipheredMessage);
    }

}
