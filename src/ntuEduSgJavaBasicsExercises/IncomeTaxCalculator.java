package ntuEduSgJavaBasicsExercises;

import java.util.Scanner;

public class IncomeTaxCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your taxable income:");
        int taxableIncomeInput = scanner.nextInt();

        calculateTaxes(taxableIncomeInput);
    }

    public static void calculateTaxes(int taxableIncome) {
        final double TAX_RATE_ABOVE_20K = 0.1;
        final double TAX_RATE_ABOVE_40K = 0.2;
        final double TAX_RATE_ABOVE_60K = 0.3;

        double payableTax;

        if (taxableIncome <= 20000) {
            payableTax = 0.0;
        } else if (taxableIncome <= 40000) {
            payableTax = (taxableIncome - 20000) * TAX_RATE_ABOVE_20K;
        } else if (taxableIncome <= 60000) {
            payableTax = (taxableIncome - 40000) * TAX_RATE_ABOVE_40K + 20000 * TAX_RATE_ABOVE_20K;
        } else  {
            payableTax = (taxableIncome - 60000) * TAX_RATE_ABOVE_60K + 20000 * TAX_RATE_ABOVE_40K + 20000 * TAX_RATE_ABOVE_20K;
        }

        System.out.printf("Payable taxes are as follows: EUR %.2f", payableTax);


    }
}
