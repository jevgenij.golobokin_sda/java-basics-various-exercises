package smallProjects;

import java.util.Random;
import java.util.Scanner;

public class RockPaperScissorsGame {

    static Scanner scanner = new Scanner(System.in);
    static Random random = new Random();

    public static void main(String[] args) {

        boolean gameIsRunning = true;
        System.out.println("Hi! This is Rock, Paper, Scissors Game! Can you beat a computer?");
        System.out.println("1 Yeah, I'm ready! \n2 Hold on a minute! What are the rules?");

        while (gameIsRunning) {
            String startOrPrintRules = scanner.nextLine();

            if (startOrPrintRules.equals("1")) {
                launchGame();
                gameIsRunning = false;
            }
            if (startOrPrintRules.equals("2")) {
                printRules();
            }
            if (!startOrPrintRules.equals("1") && !startOrPrintRules.equals("2")) {
                System.out.println("Invalid selection!");
                System.out.println("1 Start a game \n2 Read the rules");
            }
        }
    }

    public static void launchGame() {
        String userSelection;
        String[] computerSelectionsArray = {"rock", "paper", "scissors"};

        int userScore = 0;
        int computerScore = 0;
        int matchCounter = 1;

        while (userScore < 5 && computerScore < 5) {
            String computerSelection = computerSelectionsArray[random.nextInt(computerSelectionsArray.length)];

            System.out.println("Match " + matchCounter + "! Select your shape: Rock, Paper or Scissors?");
            userSelection = scanner.nextLine().toLowerCase();

            //check if user and computer choices are same
            if (computerSelection.equals(userSelection)) {
                printChoices(userSelection, computerSelection);
                System.out.println("Draw!");
                printScore(userScore, computerScore);
                continue;
            }

            //check if invalid choice
            if (!userSelection.equals("rock") && !userSelection.equals("paper") && !userSelection.equals("scissors")) {
                System.out.println("Invalid choice! Next match!");
            }

            //user choice is ROCK
            if (userSelection.equals("rock")) {
                if (computerSelection.equals("paper")) {
                    computerScore++;
                    printChoices(userSelection, computerSelection);
                    System.out.println("Computer wins!");
                    printScore(userScore, computerScore);
                }
                if (computerSelection.equals("scissors")) {
                    userScore++;
                    printChoices(userSelection, computerSelection);
                    System.out.println("You win!");
                    printScore(userScore, computerScore);
                }
            }

            //user choice is PAPER
            if (userSelection.equals("paper")) {
                if (computerSelection.equals("scissors")) {
                    computerScore++;
                    printChoices(userSelection, computerSelection);
                    System.out.println("Computer wins!");
                    printScore(userScore, computerScore);
                }
                if (computerSelection.equals("rock")) {
                    userScore++;
                    printChoices(userSelection, computerSelection);
                    System.out.println("You win!");
                    printScore(userScore, computerScore);
                }
            }

            //user choice is SCISSORS
            if (userSelection.equals("scissors")) {
                if (computerSelection.equals("rock")) {
                    computerScore++;
                    printChoices(userSelection, computerSelection);
                    System.out.println("Computer wins!");
                    printScore(userScore, computerScore);
                }
                if (computerSelection.equals("paper")) {
                    userScore++;
                    printChoices(userSelection, computerSelection);
                    System.out.println("You win!");
                    printScore(userScore, computerScore);
                }
            }

            matchCounter++;
        }

        if (userScore > computerScore) {
            System.out.println("Congratulations!! You win the game!");
            printScore(userScore, computerScore);
        } else {
            System.out.println("Oh noes! You lose! Hope you can beat this super smart computer next time..");
            printScore(userScore, computerScore);
            System.out.println("See you nex time!!");
        }


    }

    public static void printRules() {
        System.out.println("After you start a game, you and computer simultaneously select \n" +
                "one of three shapes:  \"rock\", \"paper\", or \"scissors\". \n" +
                "The winner is then selected based on standard game rules: \n" +
                "rock crushes scissors; paper covers rock; scissors cuts paper. \n" +
                "The winner gets one point, and if match ends in draw none of the players \n" +
                "gets a point. The game is played till one of players wins by collecting \n" +
                "5 points.");
        System.out.println();
        System.out.println("1 Great! Let's play! \n2 Wait, wait, I didn't get the rules!");
    }

    public static void printChoices(String userSelection, String computerSelection) {
        System.out.println("Your choice: " + userSelection + "\nComputer's choice: " + computerSelection);
    }

    public static void printScore(int userScore, int computerScore) {
        System.out.println("Your score: " + userScore + "   Computer's score: " + computerScore);
        System.out.println();
    }
}
