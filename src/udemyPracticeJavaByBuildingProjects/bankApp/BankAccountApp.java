package udemyPracticeJavaByBuildingProjects.bankApp;

import utilities.CSVReader;

import java.util.LinkedList;
import java.util.List;

public class BankAccountApp {



    public static void main(String[] args) {

        List<Account> accounts = new LinkedList<>();

        //Read a csv file and create account based on that data
        String filePath = "NewBankAccount.csv";
        List<String[]> newAccountHolders = CSVReader.read(filePath);

        for (String[] accountHolder : newAccountHolders) {
            String name = accountHolder[0];
            String sSN = accountHolder[1];
            String accountType = accountHolder[2];
            double initDeposit = Double.parseDouble(accountHolder[3]);

            if (accountType.equals("Savings")) {
                accounts.add(new Savings(name, sSN, initDeposit));
            } else if (accountType.equals("Checking")) {
                accounts.add(new Checking(name, sSN, initDeposit));
            } else {
                System.out.println("Error reading account type");
            }
        }

        for (Account account : accounts) {
            System.out.println("\n############");
            account.showInfo();
        }

    }

}
