package udemyPracticeJavaByBuildingProjects.studentDbApp;

import java.util.Scanner;

public class Student {
    private String firstName;
    private String lastName;
    private int gradeYear;
    private String studentID;
    private String courses = "";
    private int tuitionBalance = 0;
    private static int costOfCourse = 600;
    private static int id = 1000;

    // Constructor prompt user to enter name and year
    public Student() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter student First Name:");
        this.firstName = scanner.nextLine();

        System.out.println("Enter student Last Name:");
        this.lastName = scanner.nextLine();

        System.out.println("Enter student Grade level:\n1 - Freshman\n2 - Sophmore\n3 - Junior\n4 - Senior");
        this.gradeYear = scanner.nextInt();

        setStudentID();

    }

    // Generate ID
    private void setStudentID() {
        // Grade level + ID (static)
        id++;
        this.studentID = gradeYear + "" + id;
    }

    // Enroll in courses
    public void enroll() {

        do {
        System.out.println("Enter course to enroll (Q to quit): ");
        Scanner scanner = new Scanner(System.in);
        String course = scanner.nextLine();
        if (!course.equals("Q")) {
            courses = courses + "\n  " + course;
            tuitionBalance = tuitionBalance + costOfCourse;
        } else {
            break;
        }
        } while (true);

    }

    // View balance and pay tuition
    public void viewBalance() {
        System.out.println("Your balance is: " + tuitionBalance + "EUR");
    }

    public void payTuition() {
        viewBalance();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter amount to pay, EUR: ");
        int payment = scanner.nextInt();
        tuitionBalance = tuitionBalance - payment;
        System.out.println("Thank you for your payment of " + payment + "EUR");
        viewBalance();
    }

    // Print status
    public String toString() {
        return "Name: " + firstName + " " + lastName + "\nStudent ID: " + studentID +
                "\nGrade level: " + gradeYear + "\nCourses enrolled:" + courses +
                "\nTuition balance:\n" + tuitionBalance + "EUR";
    }
}
