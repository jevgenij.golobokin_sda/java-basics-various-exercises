package udemyPracticeJavaByBuildingProjects.studentDbApp;

import java.util.Scanner;

public class StudentDBApp {
    public static void main(String[] args) {
        // Ask how many users to add

        System.out.println("Enter number of new students to enroll: ");
        Scanner scanner = new Scanner(System.in);
        int numberOfStudents = scanner.nextInt();
        Student[] students = new Student[numberOfStudents];


        // Create n number of new students
        for  (int i = 0; i < numberOfStudents; i++) {
            students[i] = new Student();
            students[i].enroll();
            students[i].payTuition();
            System.out.println(students[i].toString());
            System.out.println("----------------------");
        }


        // changes:
        // courses - online coding classes
        // upon payment - export student data to file
        // let user select classes from the list
        // different costs for different classes - use final int field?

    }
}
